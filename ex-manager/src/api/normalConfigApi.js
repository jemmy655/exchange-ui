import request from '@/utils/request';

export const normalConfigApi = {
  /**
   * 新增银行卡
   * @param data
   */
  createBank(data) {
    return request({
      url: '/adminBank/create',
      method: 'post',
      data
    });
  },

  /**
   * 编辑状态
   * @param data
   */
  setBanks(params) {
    return request({
      url: '/adminBank/adminUpdateBankStatus',
      method: 'post',
      params: params
    });
  },


  /**
   * 获取银行卡列表
   * @param form
   * @param current
   * @param size
   */
  getBankList(form, current, size) {
    let params = {};
    params["bankCard"] = form["bankCard"];
    params.current = current;
    params.size = size;
    return request({
      url: '/adminBank/getList',
      method: 'get',
      params: params
    });
  },

  /**
   * 更新银行卡
   * @param data
   */
  updateBank(data) {
    return request({
      url: '/adminBank/update',
      method: 'post',
      data
    });
  },

  /**
   * 新增配置
   * @param data
   */
  createConfig(data) {
    return request({
      url: '/config/create',
      method: 'post',
      data
    });
  },


  /**
   * 获取配置列表
   * @param current
   * @param size
   */
  getConfigList(current, size, form = {}) {
    let params = {
      ...form,
    };
    params.current = current;
    params.size = size;
    return request({
      url: '/config/getList',
      method: 'get',
      params: params
    });
  },

  /**
   * 更新配置
   * @param data
   */
  updateConfig(data) {
    return request({
      url: '/config/update',
      method: 'post',
      data
    });
  },

  /**
   * 新增系统权限
   **/
  createSysPrivileges(data) {
    return request({
      url: '/privileges',
      method: 'post',
      data: data
    })
  },

  /**
   * 编辑系统权限
   */
  editSysPrivileges(data) {
    return request({
      url: '/privileges',
      method: 'patch',
      data: data
    })
  },

};
