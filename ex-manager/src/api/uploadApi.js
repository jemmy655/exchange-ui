import request from '@/utils/request'
import axios from 'axios'

export const uploadApi = {
  // 此接口不带admin前缀
  getPreUpload() {
    return axios({
      url: `${process.env.BASE_API}/v2/s/preupload`,
      method: 'get'
    })
  }
}
