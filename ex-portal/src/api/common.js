import request from "./request";

/*
 * 公共接口 V3
 */
export const commonApi = {
  SMS_TYPE: {
    //修改手机号码
    CHANGE_PHONE_VERIFY: "CHANGE_PHONE_VERIFY",
    //修改登录密码
    CHANGE_LOGIN_PWD_VERIFY: "CHANGE_LOGIN_PWD_VERIFY",
    //修改交易密码
    CHANGE_PAY_PWD_VERIFY: "CHANGE_PAY_PWD_VERIFY",
    //用户手机注册验证
    REGISTER_VERIFY: "REGISTER_VERIFY",
    // 用户找回密码验证
    FORGOT_VERIFY: "FORGOT_VERIFY",
    // 用户找回交易密码
    FORGOT_PAY_PWD_VERIFY: "FORGOT_PAY_PWD_VERIFY",
    // 注册成为代理商用户
    REGISTER_AGENT: "REGISTER_AGENT",
    // 线下充值拒绝短信
    UNDER_LINE_REFUSE: "UNDER_LINE_REFUSE",
    // 线下充值成功短信
    UNDER_LINE_SUCCESS: "UNDER_LINE_SUCCESS",
    // 提币申请
    WITHDRAW_APPLY: "WITHDRAW_APPLY",
    // 提币成功
    WITHDRAW_SUCCESS: "WITHDRAW_SUCCESS",
    // 提现申请
    CASH_WITHDRAWS: "CASH_WITHDRAWS",
    // 登录
    LOGIN: "LOGIN",
    // 验证老手机或 邮箱
    VERIFY_OLD_PHONE:"VERIFY_OLD_PHONE"

  },
  checkMobile(mobile, countryCode) {
    return request({
      url: '/v2/u/user/checkTel',
      method: 'get',
      params: {mobile, countryCode}
    })
  },
  checkEmail(email) {
    return request({
      url: '/v2/u/user/checkEmail',
      method: 'get',
      params: {email}
    })
  },

  // 用户信息
  serverUserinfo(token) {
    return request({
      url: '/v2/u/user/info',
      method: 'get',
      headers: {
        'Authorization': token,
      },
    })
  },

  // 刷新token
  refreshToken(token) {
    return request({
      url: '/v2/u/refreshToken',
      method: 'get',
      headers: {
        'Authorization': token,
      },
    })
  },

  // 改变语言
  changeLanguage(lang) {
    return request({
      url: '/v2/u/user/lang?lang=' + lang
    })
  },

  sendSms(phone, templateCode, countryCode = '+86', validateType) {
    let data = {}
    if (validateType === 1) {
      data = {email: phone, templateCode}
    } else {
      data = {phone, templateCode, countryCode}
    }
    return request({
      url: '/v2/s/api/v1/dm/sendTo',
      method: "post",
      data: data
    })
  },
  // 提交工单
  addWorkIssue(question, token) {
    return request({
      url: '/v2/s/workIssue/addWorkIssue',
      method: "post",
      headers: {
        'Authorization': token,
      },
      data: {
        question
      }
    })
  },

  // 获取工单列表
  getWorkIssueList(current, size, token) {
    return request({
      url: `/v2/s/workIssue/issueList/${current}/${size}`,
      method: 'get',
      headers: {
        'Authorization': token
      }
    })
  }
}

