import request from "./request";
import axios from 'axios'
let qs  = require('qs')

export const homeApi = {
  // 新的首页 包含交易区的market接口
  getMarketListNew(){
    return request({
      url : '/v2/s/trade/market/area',
      method : 'get',
    })
  },
  getMarketList(){
    return request({
      // url    : '/v2/s/trade/market/all',
      url    : '/v2/s/trade/market',
      method : 'get',
    }).then((res) => {
      return Promise.resolve(res.data)
    })
  },
  // 获取banner
  getBanner(){
    return request({
      url : '/v2/s/home/other/banner',
      method : 'get'
    })
  },

  getDoucments(){
    return request({
      url : '/v2/s/home/other/documents',
      method : 'get'
    })
  },
  getNoticeList (current,size) {
    return request({
      url : `/v2/s/notice/${current}/${size}`,
      method : 'get'
    })
  },
  getNoticeDetail (noticeId) {
    return request({
      url : `/v2/s/notice/${noticeId}`,
      method : 'get'
    })
  }
}
