import request from './request'

export const uploadApi = {
  aliyunUrl: process.env.BASE_API + "/v2/s/image/AliYunImgUpload",
  normalUrl: process.env.BASE_API + "/v2/s/image/commonImgUpload",

  getPreUpload() {
    return request({
      url: `/v2/s/preupload`,
      method: 'get'
    })
  }
};
