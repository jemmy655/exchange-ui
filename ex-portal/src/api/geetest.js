import gtInit from './geetest.gt'
import request from "./request";

export default {
  getGtCaptcha () {
    let url = '/v2/u/gt/register';
    return request({
      method: 'get',
      url: url,
      withCredentials: true
    })
  },
}
